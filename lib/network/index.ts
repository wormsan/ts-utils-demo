import { Request, Response, RequestHandler } from 'express'
import axios from 'axios'
interface SSRContext {
  req: Request,
  res: Response,
}
type PossibleSSRContext = SSRContext | Window

// https://www.typescriptlang.org/docs/handbook/2/narrowing.html#using-type-predicates
function isSSRContext (ctx: PossibleSSRContext) : ctx is SSRContext {
  // not a best practice
  if ((ctx as any).req && (ctx as any).res) {
    return true
  } else {
    return false
  }
}

interface IService<TReqData, TResData> {
  request (params?: TReqData) : Promise<TResData>
}


// https://www.typescriptlang.org/docs/handbook/release-notes/typescript-3-1.html#mapped-types-on-tuples-and-arrays
type TResults<T extends IService<unknown, unknown>[]> = {
  [K in keyof T]: T[K] extends IService<unknown, infer R> ? R : unknown
}

abstract class Service<TReqData, TResData> implements IService<TReqData, TResData> {
  protected params: TReqData
  protected ctx?: SSRContext
  constructor (params: TReqData) {
    this.params = params
  }
  // 如果不想暴露的话，也可以使用Proxy/Reflect做元编程
  addContext (ctx: SSRContext) {
    this.ctx = ctx
  }
  protected async fetch (url: string, data: TReqData) {
    // 这里可以在ssr中处理公共逻辑
    // 可以在这里拓展一些中间件机制进行公共逻辑操作
    if (this.ctx) {
      this.ctx.req.get('cookie')
    }
    return axios.post<TResData>(url, data).then((rawData) => {
      return rawData.data
    })
  }
  abstract request (params?: TReqData) : Promise<TResData>
}


class ServiceHelper {
  ctx: PossibleSSRContext
  constructor (ctx: PossibleSSRContext) {
    this.ctx = ctx
    this.handleServices = this.handleServices.bind(this)
  }
  async handleServices <TServices extends IService<unknown, unknown>[]>(...services: [...TServices]) : Promise<[...TResults<TServices>]> {
    let requests = services.map((service) => {
      if (isSSRContext(this.ctx)) {
        (service as Service<unknown, unknown>).addContext(this.ctx)
      }
      return service.request()
    })
    return Promise.all(requests).then((results) => {
      // 这里需要帮tsc一把
      return results as [...TResults<TServices>]
    })
  }
}

function handleServices (ctx: PossibleSSRContext) {
  return async function <TServices extends IService<unknown, unknown>[]>(...services: [...TServices]) : Promise<[...TResults<TServices>]> {
    const helper = new ServiceHelper (ctx)
    return helper.handleServices(...services).then((result) => {
      return result as [...TResults<TServices>]
    })
  }
}
interface RequestOfFoo {
  req: string
}

interface ResponseOfFoo {
  foo: string,
  foo2: number
}


interface RequestOfBar {
  reqBar: string
}

interface ResponseOfBar {
  bar: number,
  bar2: string
}
class FooService extends Service<RequestOfFoo, ResponseOfFoo> {
  request () {
    return this.fetch('/fetch_foo', this.params)
  }
}

class BarService extends Service<RequestOfBar, ResponseOfBar> {
  request () {
    return this.fetch('/fetch_bar', this.params)
  }
}

async function requestSth (ctx: PossibleSSRContext) {
  const [fooResult, barResult] = await handleServices(ctx)(new FooService({req: 'sth'}), new BarService({reqBar: 'sth'}))
  return {fooResult, barResult}
}
// ssr
const middleware: RequestHandler = (req, res, next) => {
  requestSth({req, res})
}
// client side
requestSth(window)