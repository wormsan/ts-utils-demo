import { RequestHandler } from 'express'
import axios from 'axios'
interface RequestOfFoo {
  req: string
}

interface ResponseOfFoo {
  foo: string,
  foo2: number
}


interface RequestOfBar {
  reqBar: string
}

interface ResponseOfBar {
  bar: number,
  bar2: string
}

async function requestSth () {
  const fooReq = axios.post<ResponseOfFoo>('/fetch_foo', {
    reqFoo: 'sth'
  })
  const barReq = axios.post<ResponseOfBar>('/fetch_bar', {
    reqBar: 'sth'
  })
  return Promise.all([fooReq, barReq]).then(([fooRes, barRes]) => {
    return {
      fooResult: fooRes.data,
      barResult: barRes.data
    }
  })
}


// ssr
const middleware: RequestHandler = (req, res, next) => {
  // ???
}